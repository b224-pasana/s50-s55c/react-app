import { useState, useEffect } from "react";
import { Row, Col, Card, Button } from "react-bootstrap";
import { Link } from 'react-router-dom'

export default function CourseCard({courseProp}) {

    // Checks to see if the data was successfully passed.
    // console.log(props)
    // Every component receives information in a form of an object.
    // console.log(typeof props)
    // Using the dot notation, access the property to retrieve the value/data.
    // console.log(props.courseProp.name)
    // Checks if we can retrieve the data from courseProp
    // console.log(courseProp)

    /*
        Use the state hook for this component to be able to store itrs state. 
        States are used to keep track of the information related to individual
        components.

        Syntax:
            const [getter, setter] = useState(initialGetterValue)
    */

            const [count, setCount] = useState(0);
            // console.log(useState(0));

            const [seats, setSeats] = useState(30);

            // function enroll() {

            //     setCount(count + 1)
            //     setSeat(seats - 1)

            //     console.log('Enrollee: ' + count)
            //     console.log('Seats: ' + seats)

            //         if(count === 30) {
            //             setCount(count)
            //             setSeat(seats)
            //             alert("No more seats available.")
            //         }
            // }

            // function cancelEnroll() {

            //     setCount(count - 1)
            //     setSeat(seats + 1)

            //         if(count === 0) {
            //             setCount(count)
            //             alert("There were no enrollees to un-enroll.")
            //         }

            //     console.log('Enrollee: ' + count)
            //     console.log('Seats: ' + seats)
            // }

            function enroll() {
                if(count < 30) {
                    setCount(count + 1)
                    // console.log('Enrollee: ' + count)
                    setSeats(seats - 1)
                    // console.log('Seats: ' + seats)
                } 
                // else {
                //     alert("No more seats available.")
                // }
            }

            useEffect(() => {
                if(seats === 0) {
                    alert("No more seats available.")
                }
            })

    // Object destructuring
    const { name, description, price, _id } = courseProp;

    return (

        <Row>
            <Col xs={12} md={6}>
                <Card className="courseCard">
                        <Card.Body>
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text> 
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php {price}</Card.Text>
                            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>
                            Details
                            </Button>
                            {/* <Button className="m-2 bg-primary" onClick={cancelEnroll}>
                            Un-enroll
                            </Button> */}
                        </Card.Body>
                    </Card>
            </Col>
                
        </Row>
    )
}
