import { Row, Col, Button } from "react-bootstrap";

export default function BannerError() { 

    return (

        <Row>
            <Col className="p-5">
                <h1>Error 404 - Page Not Found</h1>
                <p>The page you are looking for.</p>
                <Button variant="primary">Back to Homepage!</Button>
            </Col>
        </Row>
    )
}