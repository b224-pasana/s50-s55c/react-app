import { useEffect, useState } from "react";
// import components
import CourseCard from "../components/CourseCard";
// import data
// import coursesData from "../data/coursesData"; -->> MOCK DATA

export default function Courses() {

    const [courses, setCourses] = useState([])

    // Check to see if the mock data was captured.
    // console.log(coursesData);
    // console.log(coursesData[0]);

        useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/courses/`)
            .then(res => res.json())
            .then(data => {
                console.log(data)
                setCourses(data.map(course => {
                    return (
                        <CourseCard key={course._id} courseProp={course} />
                    )
                }))
        })
       
        }, [])
        


    return (

        <>
            <h1>Courses</h1>
            {courses}
        </>

    )
}

