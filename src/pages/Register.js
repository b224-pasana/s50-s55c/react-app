import { useState, useEffect, useContext } from "react";
import { Form, Button} from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';
import UserContext from "../UserContext";

export default function Register() {

    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();


    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // state to determine whther the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(firstName)
    console.log(lastName)
    console.log(email)
    console.log(mobileNo)
    console.log(mobileNo.length)
    console.log(email)
    console.log(password1)
    console.log(password2)
    

    function emailExists(e) {

            e.preventDefault()

            fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
                method: 'POST', 
                headers: {
                    'Content-Type': 'application/json'
                }, 
                body: JSON.stringify({
                    email: email

                })
            }) 
            . then(res => res.json()) 
            .then(data => {
                
                console.log(data);

                if(data === false) {
    
                    // simulate user registration
                        fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                            method: 'POST', 
                            headers: {
                                'Content-Type': 'application/json'
                            }, 
                            body: JSON.stringify({
                                firstName: firstName, 
                                lastName: lastName,
                                email: email,
                                mobileNo: mobileNo,
                                password: password1

                            })
                        }) // FIRST!(1) <<----
                        . then(res => res.json()) //<-- SECOND! (2)
                        .then(data => {
                            // we will receive either a token or a false response
                            console.log(data);

                            if(data === true) {
                    
                                Swal.fire({
                                    title: "Welcome to Zuitt!",
                                    icon: "success",
                                    text: "You have successfully registered!"
                                })

                                navigate("/login");

                            } else {

                                Swal.fire({
                                    title: "Soumething went wrong.",
                                    icon: "error",
                                    text: "Please try again."
                                })
                            }

                        }); 


                        // Clearing the input fields and states
                        setFirstName("");
                        setLastName("");
                        setMobileNo("");
                        setEmail("");
                        setPassword1("");
                        setPassword2("");

    
                } else {
    
                    Swal.fire({
                        title: "Duplicate email found.",
                        icon: "error",
                        text: "Please provide a different email address."
                    })
                }
    
            }); 
    

        }

 
    useEffect(() => {
        if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo.length === 11) && ( password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2, firstName, lastName, mobileNo]);

    return (

            
        (user.id !== null) ? 
  
            <Navigate to="/courses" />
            :
            <Form onSubmit={e => emailExists(e)}>
                <h1 className="text-center">Sign Up</h1>
                 <Form.Group className="mb-3" controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="First Name"
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Last Name"
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Mobile Number"
                        value={mobileNo}
                        onChange={e => setMobileNo(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password"
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password"
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>
            
            {
                isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
            }
        </Form>
    )
}